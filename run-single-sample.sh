#!/usr/bin/env bash
set -e

logmessage(){
    message="$1"
    >&2 echo -e "$( date )\t$RunID\t$message"
}
# Install path for other scripts
installpath="$( dirname "$(readlink -f "$0")" )"
installpath="$installpath"/scripts

# Load config option
. "$installpath"/*.config

# Load a variables file to get analysis directories
. *.variables

sample="$( basename $( pwd ) )"

## Analysis
## --------

# Use rsync rather than cp, so it won't update if there have
# been no changes
rsync -a "$installpath"/*.sh .
rsync -a "$installpath"/WRGLPipeline.slurm .
rsync -a "$installpath"/WRGLPipeline.config .
# R script for CNV calling gets copied into the run-level folder
rsync -a "$installpath"/*.r ..
rsync -a "$BEDFilename" ..

# This should ensure that the job is always submitted to
# the quietest node.
targetnode="$( $scriptdir/idle-node-finder )"

sbatchid="$( sbatch \
               --nodes=$alignmentnodes \
               --ntasks=$alignmentntasks \
               --time=$alignmenttime \
               --mem=$alignmentmem \
               --nodelist=$targetnode \
               --parsable WRGLPipeline.slurm )"


logmessage "INFO: Submitted alignment jobs for sample $sample"
