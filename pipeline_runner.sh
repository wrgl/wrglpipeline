#!/usr/bin/env bash
set -e

logmessage(){
    message="$1"
    >&2 echo -e "$( date )\t$RunID\t$message"
}
# Install path for other scripts
installpath="$( dirname "$(readlink -f "$0")" )"
scriptspath="$installpath"/scripts

# Load config option
. "$scriptspath"/*.config

# Load a variables file to get analysis directories
. */*.variables

# Load the list of compute nodes to exlude
# Due to ssdfs and module load issues
. "$installpath"/nodes-to-exclude.txt

logmessage "INFO: Run analysis started."


## Analysis
## --------

for sample in "${AnalysisDirs[@]}"; do
    # Copy in scripts and config file and submit job
    cd "$sample"
    # Use rsync rather than cp, so it won't update if there have
    # been no changes
    rsync -a "$scriptspath"/*.sh .
    rsync -a "$scriptspath"/WRGLPipeline.slurm .
    rsync -a "$scriptspath"/WRGLPipeline.config .
    # R script for CNV calling gets copied into the run-level folder
    rsync -a "$scriptspath"/*.r ..
    rsync -a "$sample"/"$BEDFilename" ..

    # This doesn't seem to maintain permissions, so we have to make the scripts executable
    chmod 770 *.sh
    chmod 770 ../*.r

    # This should ensure that the job is always submitted to
    # the quietest node.
    # A short sleep should ensure we are accounting for these jobs
    # as they are submitted
#    sleep 0.1
#    targetnode="$( $scriptdir/idle-node-finder )"

    # DEV: Rather than specifying the node to run on, trying to specify nodes to *exclude*
    #      (i.e. with known issues), and let slurm choose the best node for running.
    sbatchid="$( sbatch \
                   --nodes=$alignmentnodes \
                   --ntasks=$alignmentntasks \
                   --time=$alignmenttime \
                   --mem=$alignmentmem \
                   --exclude=$excludenodes \
                   --parsable WRGLPipeline.slurm \
                   --partition=highmem )"
#                   --nodelist=$targetnode \

    logmessage "INFO: Submitted alignment jobs for sample $sample"
    cd ..
done

# DEV: If needed, we could fire off the run-level stuff here as a separate
#      job using the SBATCH dependency option? Depends on if the total runtime
#      is ok within the time allowed by the per-sample jobs.


## Dependency examples
## ----

# DEV: Array to hold submitted job IDs for slurm dependency
#jobids=()

# Create a colon-separated dependency list from the job ids
#deps=$( IFS=$':'; echo "${jobids[*]}" )

#echo "$deps"

#cp "$installpath"/*.sh .
#cp "$installpath"/*.config .
# DEV: Might need to also copy config and a variables file here?
#      Hopefully these should all copy before the script starts to run...
#sbatch --parsable --dependency=afterok:"$deps" --partition batch 2_quality_recalibration.sh

## DEV
## ---

# If there is no internet connection from the compute nodes, then we may need to keep
# this script open, in order to update the QC database.
