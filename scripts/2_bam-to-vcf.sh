#!/bin/bash

# #SBATCH --job-name=CallVariants
# #SBATCH --output=%x.%j.out
# #SBATCH --error=%x.%j.err

# Script details
# ==============


# Description: WRGL Pipeline
# Stage: 2 - Call variants
# Mode: BY_SAMPLE
# Author: Ben Sanders
# Maintainer: ben.sanders@salisbury.nhs.uk
# Status: DEVELOPMENT
# Updated 05 October 2021, Ben Sanders
# Update note: Transfer to Iridis 5 initiated major re-write
#              to account for new job scheduler and also software
#              availability - e.g. update to GATK4


# Script config
# =============


# Load pipeline settings - load before variables and any duplicate valures
#                          will be replaced with the variables file ones.
. WRGLPipeline.config

# Load sample variables
. *.variables

cleanup(){
    # Run in event of error - delete temp files from /ssdfs
    # EXIT seems to be the most flexible option - it catches errors and ctrl+c
    # without issues, vs specifying EXIT and ERR, which can repeat itself.
    >&2 echo 'DEV: Run cleanup here if needed...'
}
trap 'cleanup' EXIT

logmessage(){
    # Print a message to stderr, with date + time stamp
    message="$1"
    >&2 echo -e "$( date )\t$RunID\t$Sample_ID\t$message"
}

# Pre-processing checks
# =====================


# Check BAM file input exists
# ---------------------------

if [ ! -f "$RunID"_"$Sample_ID".bam ]; then
    logmessage "ERROR: Cannot find BAM file for sample "$Sample_ID"."
    exit 1
fi

# Check if output files already exist
# -----------------------------------

# Check the final BAM file exists
logmessage "INFO: Checking if VCF needs re-generating..."
if [ -f "$RunID"_"$Sample_ID".bam ]; then
  # Then check that VCF is newer than the input BAM, scripts, etc.
  if [ "$RunID"_"$Sample_ID".vcf -nt "$RunID"_"$Sample_ID".bam ] && \
     [ "$RunID"_"$Sample_ID".vcf -nt "$varcallingscript" ] && \
     [ "$RunID"_"$Sample_ID".vcf -nt "$Sample_ID".variables ] && \
     [ "$RunID"_"$Sample_ID".vcf -nt *.config ]
  then
    # If the BAM is newer there have been no changes and there is no
    # need to repeat the analysis, so we can exit here.
    logmessage "INFO: VCF file already exists and does not require repeating."
    exit 0
  fi
  # If they are newer, we need to repeat (e.g. script change)
  logmessage "WARNING: VCF file is older than prerequisite. Repeating analysis."
fi

# BAM to VCF processing
# =======================


# Variant calling
# ---------------

# gVCF calling for coverage
logmessage "INFO: Beginning variant calling..."
"$gatk" HaplotypeCaller \
  --java-options "-Xmx8G -Djava.io.tmpdir="$javatmp"" \
  --tmp-dir "$javatmp" \
  --input "$RunID"_"$Sample_ID".bam \
  --reference "$refgenome" \
  --dbsnp "$dbsnpfile" \
  --intervals "$BEDFilename" \
  --interval-padding 100 \
  --standard-min-confidence-threshold-for-calling 30 \
  --emit-ref-confidence BP_RESOLUTION \
  --output "$RunID"_"$Sample_ID".gvcf.gz

# Genotype the gVCF
logmessage "INFO: Genotyping GVCF file..."
"$gatk" GenotypeGVCFs \
  --java-options "-Xmx8G -Djava.io.tmpdir="$javatmp"" \
  --tmp-dir "$javatmp" \
  --variant "$RunID"_"$Sample_ID".gvcf.gz \
  --reference "$refgenome" \
  --intervals "$BEDFilename" \
  --interval-padding 100 \
  --output "$RunID"_"$Sample_ID".GENOTYPED.vcf.gz


#mv "$RunID"_"$Sample_ID".GENOTYPED.vcf "$RunID"_"$Sample_ID"    .TEMP.vcf
#bgzip "$RunID"_"$Sample_ID".TEMP.vcf
#tabix -p vcf "$RunID"_"$Sample_ID".TEMP.vcf.gz

#rm "$RunID"_"$Sample_ID".GENOTYPED.vc*


# Normalise and split and multiallelic variants
# DEV: Should this happen *before* CNN scoring?
#      Try both ways and see what difference it makes?
bcftools norm \
  --fasta-ref "$refgenome" \
  --multiallelics -any \
  --threads 2 \
  --output-type v \
  --output "$RunID"_"$Sample_ID".NORMALISED.vcf \
  "$RunID"_"$Sample_ID".GENOTYPED.vcf.gz

#rm "$RunID"_"$Sample_ID".TEMP.vcf.g*

# Get only the target regions from the BED file
# NOTE: Using -wa should ensure any larger indels overlapping
#       the target region are included in full.
logmessage "INFO: Extracting variants from "$BEDFilename" ROI only..."
bedtools intersect \
  -header \
  -wa \
  -a "$RunID"_"$Sample_ID".NORMALISED.vcf \
  -b "$BEDFilename" > "$RunID"_"$Sample_ID".FINAL.vcf

bgzip "$RunID"_"$Sample_ID".FINAL.vcf
tabix -p vcf "$RunID"_"$Sample_ID".FINAL.vcf.gz
rm "$RunID"_"$Sample_ID".NORMALISED.vc*

# Run Manta
# --------------

configManta.py \
--bam "$RunID"_"$Sample_ID".bam \
--referenceFasta "$refgenome" \
--exome \
--runDir manta
manta/runWorkflow.py \
--quiet \
-m local \
-j 1

# set filter to PASS
bcftools filter -i "1==2" -s "PASS" -o manta/results/variants/diploidSV_pass.vcf manta/results/variants/diploidSV.vcf.gz

# manipulate for Alissa
sed -i 's/<INS>/<GAIN>/g' manta/results/variants/diploidSV_pass.vcf
sed -i 's/SVTYPE=INS/SVTYPE=GAIN/g' manta/results/variants/diploidSV_pass.vcf
sed -i 's/ID=INS,Description=\"Insertion\"/ID=GAIN,Description=\"Gain\"/g' manta/results/variants/diploidSV_pass.vcf

# restrict SVs to ROI but retain overlapping variants
bgzip manta/results/variants/diploidSV_pass.vcf
tabix manta/results/variants/diploidSV_pass.vcf.gz
bcftools view -R "$BEDFilename" manta/results/variants/diploidSV_pass.vcf.gz | uniq > "$RunID"_"$Sample_ID"_sv.vcf
bgzip "$RunID"_"$Sample_ID"_sv.vcf
tabix "$RunID"_"$Sample_ID"_sv.vcf.gz

# add TOOL
add_tool "$RunID"_"$Sample_ID"_sv.vcf.gz Manta

# delete workspace folder
rm -fr manta/workspace

# Run MELT
# --------------

for j in ALU LINE1 SVA; do
  
  # call variants
  java -Xmx4g -jar "$meltpath" Single \
  -a \
  -ac \
  -b GL000207.1/GL000226.1/GL000229.1/GL000231.1/GL000210.1/GL000239.1/GL000235.1/GL000247.1/GL000245.1/GL000197.1/GL000203.1/GL000246.1/GL000249.1/GL000196.1/GL000248.1/GL000244.1/GL000238.1/GL000202.1/GL000234.1/GL000232.1/GL000206.1/GL000240.1/GL000236.1/GL000241.1/GL000243.1/GL000242.1/GL000230.1/GL000237.1/GL000233.1/GL000204.1/GL000198.1/GL000208.1/GL000191.1/GL000227.1/GL000228.1/GL000214.1/GL000221.1/GL000209.1/GL000218.1/GL000220.1/GL000213.1/GL000211.1/GL000199.1/GL000217.1/GL000216.1/GL000215.1/GL000205.1/GL000219.1/GL000224.1/GL000223.1/GL000195.1/GL000212.1/GL000222.1/GL000200.1/GL000193.1/GL000194.1/GL000225.1/GL000192.1 \
  -bamfile "$RunID"_"$Sample_ID".bam \
  -bowtie "$bowtiepath" \
  -e 300 \
  -exome \
  -h "$refgenome" \
  -mcmq 100 \
  -n "$rootdir"/software/MELT/MELTv2.2.2/add_bed_files/1KGP_Hg19/b37.genes.bed \
  -r 151 \
  -t "$rootdir"/software/MELT/MELTv2.2.2/me_refs/1KGP_Hg19/"$j"_MELT.zip \
  -z 1000000 \
  -w "$j"
  
  if [ -f "$j"/"$j".final_comp.vcf ]; then
    if [[ $(wc -l <"$j"/"$j".final_comp.vcf) -gt 0 ]]; then
      # manipulate VCF for alissa
      sed -i 's/##FORMAT=<ID=GL,Number=3,Type=Float,Description=\"Genotype likelihood\">/##FORMAT=<ID=GL,Number=G,Type=Float,Description=\"Genotype likelihood\">/g' "$j"/"$j".final_comp.vcf
      bcftools filter -i "1==2" -s "PASS" -o "$j"/"$j".pass.vcf "$j"/"$j".final_comp.vcf
      sed -i "s/##ALT=<ID=INS:ME:$j,Description=\"Insertion of $j element\">/##ALT=<ID=GAIN,Description=\"Gain\">/g" "$j"/"$j".pass.vcf
      sed -i "s/<INS:ME:$j>/<GAIN>/g" "$j"/"$j".pass.vcf
      sed -i "s/SVTYPE=$j/SVTYPE=GAIN/g" "$j"/"$j".pass.vcf
      bcftools reheader -s <(echo "$Sample_ID") -o "$j"/"$j".sample.vcf "$j"/"$j".pass.vcf
      grep '##' "$j"/"$j".sample.vcf > "$j"/"$j".sample.vcf.h
      echo "##INFO=<ID=END,Number=1,Type=Integer,Description=\"End position of the variant described in this record\">" >> "$j"/"$j".sample.vcf.h
      grep '#' "$j"/"$j".sample.vcf | grep -v '##' >> "$j"/"$j".sample.vcf.h
      grep -v '#' "$j"/"$j".sample.vcf > "$j"/"$j".sample.vcf.b
      awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$7"\tEND="$2";"$8"\t"$9"\t"$10}' "$j"/"$j".sample.vcf.b > "$j"/"$j".sample.vcf.b.end
      cat "$j"/"$j".sample.vcf.h "$j"/"$j".sample.vcf.b.end > "$j"/"$j".end.vcf

      # update contig list
      "$gatk" UpdateVCFSequenceDictionary \
      -V "$j"/"$j".end.vcf \
      --source-dictionary "$refgenome" \
      --output "$j"/"$j".end.vcf.gz \
      --replace

      # restrict SVs to ROI but retain overlapping variants
      bcftools view -R "$BEDFilename" "$j"/"$j".end.vcf.gz | uniq > "$RunID"_"$Sample_ID"_"$j".vcf
      bgzip "$RunID"_"$Sample_ID"_"$j".vcf
      tabix "$RunID"_"$Sample_ID"_"$j".vcf.gz

      # add TOOL
      add_tool "$RunID"_"$Sample_ID"_"$j".vcf.gz MELT

    fi
  fi
done

# Generate coverage files
# =======================


# Copy gVCF for use in artefact detection, etc.
# ---------------------------------------------

logmessage "INFO: Copying gVCF to repository folder"
# Extract the panel name from the BED file
# NOTE: This requires a consistent naming format - just base any
#       any new files on the existing ones (e.g. WRGL3_hg19_v2.bed = WRGL3)
panel=$( basename $( find . -maxdepth 1 -name "*.bed" ) | cut -d "_" -f 1 )

# Copy the gVCF file into a panel specific repository folder and compress
mkdir -p "$gvcfrepo"/"$panel"

# TODO: Confirm that including .GENOME doesn't affect the artefact scripts
cp "$RunID"_"$Sample_ID".gvcf.gz* "$gvcfrepo"/"$panel"/


# Generate coverage file
# ----------------------

logmessage "INFO: Generating coverage details.."
# Run per-sample coverage analysis
"$scriptdir"/gvcf-to-coverage "$BEDFilename" "$genomefile" "$RunID"_"$Sample_ID".gvcf.gz

# On completion, write to file for merging
find $( pwd ) -name "*.FINAL.vcf.gz" >> ../VCFsForMerging.list

logmessage "SUCCESS: Variant calling and filtering complete."
