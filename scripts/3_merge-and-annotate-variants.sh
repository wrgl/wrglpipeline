#!/bin/bash -e

#SBATCH --job-name=AnnotateVariants
#SBATCH --output=%x.%j.out
#SBATCH --error=%x.%j.err


# Script details
# ==============


# Description: WRGL Pipeline
# Stage: 3 - Merge and annotate variants
# Mode: BY_RUN
# Author: Ben Sanders
# Maintainer: ben.sanders@salisbury.nhs.uk
# Status: DEVELOPMENT
# Updated 05 October 2021, Ben Sanders
# Update note: Transfer to Iridis 5 initiated major re-write
#              to account for new job scheduler and also software
#              availability - e.g. update to GATK4


# Script config
# =============


# Load pipeline settings - load before variables and any duplicate valures
#                          will be replaced with the variables file ones.
. */WRGLPipeline.config

# Load sample variables
. */*.variables

cleanup(){
    # Run in event of error - delete temp files from /ssdfs
    # EXIT seems to be the most flexible option - it catches errors and ctrl+c
    # without issues, vs specifying EXIT and ERR, which can repeat itself.
    >&2 echo "DEV: Run cleanup here if needed..."
}
trap 'cleanup' EXIT

logmessage(){
    # Print a message to stderr, with date + time stamp
    message="$1"
    >&2 echo -e "$( date )\t$RunID\t$Sample_ID\t$message"
}


# Pre-processing checks
# =====================


# Check prior analyses have finished
# ----------------------------------

# NOTE: This is checked in the .slurm file, but kept here just in case.
if [[ ${#AnalysisDirs[@]} -ne "$( wc -l < VCFsForMerging.list )" ]]; then
    logmessage "INFO: Not all samples appear to have completed analysis"
    exit 1
fi

# Merge VCFs
# ----------

logmessage "INFO: Merging per-sample VCFs into run-level file..."
bcftools merge \
  --file-list VCFsForMerging.list \
  --missing-to-ref \
  --merge none \
  --output-type v \
  --output "$RunID".MERGED.vcf

# Annotate with SNPEff
# --------------------

logmessage "INFO: Annotating with SnpEff..."
java -Xmx4G -jar "$snpeffpath" \
  eff \
  -v "$snpeffdb" \
  -noStats \
  -no-downstream \
  -no-intergenic \
  -no-upstream \
  -no INTRAGENIC \
  -spliceSiteSize 10 \
  -onlyTr "$PreferredTranscriptsFile" \
  -noLog \
  -formatEff \
  "$RunID".MERGED.vcf > "$RunID".SNPEff.vcf


# Annotate with SpliceAI
#-----------------------

# SpliceAI is a Python program. A conda env has been created
# to control all third-parties required
# To minimisate the impact of this funcionality  in this pipeline,
# A new vcf will be created without interfering the current workflow
# This will be saved in the genecoverage.zip file that is exported to
# the Z drive available for the users. This is done at the end of this script

logmessage "INFO: Loading conda env:spliceAI"

module swap manta/1.6.0 conda/py3-latest
source  activate  /mainfs/wrgl/.conda/envs/spliceai/

# Compress
logmessage "INFO: Compressing MERGE.vcf file for  spliceAI annotation"
bgzip -c "$RunID".MERGED.vcf  > "$RunID".MERGED.vcf.gz

# Index the file with tabix
tabix -p vcf "$RunID".MERGED.vcf.gz

# Add aplicing annotation data
logmessage "INFO: Starting spliceAI"
spliceai -I "$RunID".MERGED.vcf.gz \
         -O "$RunID".SpliceAI.vcf  \
         -R "$refgenome" \
         -A "/mainfs/wrgl/reference_files/WRGLpipeline-files/WRGLpipeline-files-1.2.0/preferred_transcript_dataset_for_spliceai_v1.tsv" \
         -M 1



# The format of the  SpliceAI.vcf can be improved.
# Let's convert this into a more human-reable table with VariantsToTable

"$gatk" VariantsToTable \
        -V "$RunID".SpliceAI.vcf  \
        -F CHROM -F POS -F REF -F ALT -F SpliceAI \
        -O "$RunID".SpliceAI.txt

# Improve the table to be easily opened with Excel

# This replace SpliceAI for its headers
sed -i '1!b;s/SpliceAI/ALLELE\tTranscript\tDS_Acceptor_Gain\tDS_Acceptor_Loss\tDS_Donor_Gain\tDS_Donor_Loss\tDS_Acceptor_Gain\tDP_Acceptor_Loss\tDP_Donor_Gain\tDP_Donor_Loss/' "$RunID".SpliceAI.txt

# This split the info -F SpliceAI in different columns, one for each header
# More human-readable to be open in excel
sed 's:|:\t:g' "$RunID".SpliceAI.txt > "$RunID".SpliceAI_annotation.tsv

# SpliceAI annotation is ready here. Below after creating gene_coverage.zip
# "$RunID".SpliceAI_annotation.tsv will be copied with the preferred_transcript_dataset_for_spliceai_v1.tsv 

# Make a copy of the preferred dataset here becaouse it will be copied in the gene_coverage.zip later
ln -s /mainfs/wrgl/reference_files/WRGLpipeline-files/WRGLpipeline-files-1.2.0/preferred_transcript_dataset_for_spliceai_v1.tsv ./

# Remove tmp files
rm "$RunID".SpliceAI.txt "$RunID".MERGED.vcf "$RunID".MERGED.vcf.gz

# Deactivate conda env. to avoid conflict with other envs.
source  deactivate  /mainfs/wrgl/.conda/envs/spliceai/
module swap conda/py3-latest manta/1.6.0

# Add REVEL score
# ---------------

# TODO: Add the VEP module, update the plugin and cache paths
logmessage "INFO: Annotating with VEP & REVEL..."
vep \
  --cache \
  --dir_cache "$vepcache" \
  --dir_plugins "$vepcache"/Plugins \
  --species homo_sapiens \
  --merged \
  --assembly "$genomeversion" \
  --format vcf \
  --vcf \
  --force_overwrite \
  --plugin REVEL,"$vepcache"/Plugins/new_tabbed_revel.tsv.gz \
  --use_given_ref \
  --no_stats \
  --fork 4 \
  --offline \
  --fasta "$refgenome" \
  --exclude_predicted \
  --input_file "$RunID".SNPEff.vcf \
  --output_file TEMP."$RunID".REVEL.vcf

"$gatk" IndexFeatureFile \
  --input TEMP."$RunID".REVEL.vcf

rm "$RunID".SNPEff.vcf

# Add PASS filters
# ----------------
# NA12878 validation showed best results on unfiltered variants, so we must set all to PASS
# except for very low coverage vars (which are unreliable anyway)
"$gatk" VariantFiltration \
  --java-options "-Xmx8G -Djava.io.tmpdir="$javatmp"" \
  --tmp-dir "$javatmp" \
  --reference "$refgenome"  \
  --intervals "$BEDFilename" \
  --filter-expression "DP < 10" \
  --filter-name "LowDP" \
  --variant TEMP."$RunID".REVEL.vcf \
  --output TEMP."$RunID".FILTERED.vcf

rm TEMP."$RunID".REVEL.vc*

# Extract REVEL score
# -------------------

logmessage "INFO: Extracting REVEL score into own field..."
"$scriptdir"/extract-revel-score \
  TEMP."$RunID".FILTERED.vcf \
  > "$RunID"_Filtered_Annotated.vcf

rm TEMP."$RunID".FILTERED.vc*


# Combine coverage information
# ============================


# Create run-level coverage file create a BAMsforDepthAnalysis.list with the same order as the coverage file
find $( pwd ) -name "*.coverage.txt" | sed s/".coverage.txt"/".bam"/g > BAMsforDepthAnalysis.list

# Collate individual sample coverage files Use BAMsforDepthAnalysis to ensure they are in the right order for
# downstream analysis Merges the files and prints the chr + pos columns, then the 3rd depth column from each
paste $( find $( pwd ) -name "*.coverage.txt" ) | \
  awk '{printf "%s\t%s",$1,$2; for (i=3;i<=NF+1;i+=3){printf"\t%s",$i;} print ""}' > "$RunID"_Coverage.txt

# If running a WRGL2 panel, we want to use a transcript-specific coverage BED file Otherwise,
# we just want to use the standard BED file. Check the first part of the BED file being
# used - if it's WRGL2 then don't do anything - the coverage file is defined in the config file.
# Otherwise (for any other panel), set coveragebed to the general run BED file, as it is already specific enough.
if [[ $( echo "$BEDFilename" | cut -d "_" -f 1 ) != "WRGL2" ]]; then
    >&2 echo INFO: Not using WRGL2, coverage bed can be set to run bed file.
    coveragebed="$BEDFilename"
fi

# DEV: This has not been moved to Iridis5 yet...
#      Should also be renamed on disk and on GitLab.
# Run coverage analysis on the transcript specific bed file
"$scriptdir"/coverage-iridis "$coveragebed" "$RunID"_Coverage.txt 20 -c "$genomefile"

logmessage "INFO: Coverage analysis complete."

logmessage "INFO: Save spliceAI data and the dataset used in the zip file (Genecoverage)"
zip "$RunID"_genecoverage.zip "$RunID".SpliceAI_annotation.tsv
zip "$RunID"_genecoverage.zip ./preferred_transcript_dataset_for_spliceai_v1.tsv

# Remove tmp files
# rm "$RunID".SpliceAI.txt 

# Complete the analysis
# =====================

# chmod everything again, so that all created files are acessible
# Add || true to force a zero exit code in case of errors (expected)
# This allows different users to queue re-runs of scripts
chmod --silent -f -R 770 . || true
chgrp --silent -f -R wrgl . || true
