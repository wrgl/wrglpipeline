#!/bin/bash -e

# #SBATCH --job-name=AlignReads
# #SBATCH --output=%x.%j.out
# #SBATCH --error=%x.%j.err

# Script details
# ==============


# Description: WRGL Pipeline
# Stage: 1 - Read alignment
# Mode: BY_SAMPLE
# Author: Ben Sanders
# Maintainer: ben.sanders@salisbury.nhs.uk
# Status: DEVELOPMENT
# Updated 05 October 2021, Ben Sanders
# Update note: Transfer to Iridis 5 initiated major re-write
#              to account for new job scheduler and also software
#              availability - e.g. update to GATK4


# Script config
# =============


# Load pipeline settings - load before variables and any duplicate valures
#                          will be replaced with the variables file ones.
. WRGLPipeline.config

# Load sample variables
. *.variables

cleanup(){
    # Run in event of error - delete temp files from /ssdfs
    # EXIT seems to be the most flexible option - it catches errors and ctrl+c
    # without issues, vs specifying EXIT and ERR, which can repeat itself.
    logmessage "INFO: Deleting temp files from /ssdfs..."
    rm -rf "$javatmp"/"$RunID"_"$Sample_ID"*.*
    rm -rf "$ssdpath"/"$RunID"_"$Sample_ID"*.*
}
trap 'cleanup' EXIT

logmessage(){
    # Print a message to stderr, with date + time stamp
    message="$1"
    >&2 echo -e "$( date )\t$RunID\t$Sample_ID\t$message"
}


# Pre-processing checks
# =====================


# Check md5 files for complete file transfer
# ------------------------------------------

# Loop through all md5 files in the folder (lets us add more if we want)
logmessage "INFO: Checking MD5 files..."
for md5 in $( find . -mindepth 1 -maxdepth 1 -name "*.md5" ); do
    # Check the exit code of the md5sum command
    if ! md5sum --status --strict -c "$md5"; then
        logmessage "ERROR: md5 checksum for "$md5" does not match file."
        exit 1
    fi
done

# Check if output files already exist
# -----------------------------------

# Check the final BAM file exists
logmessage "INFO: Checking if BAM needs re-generating..."
if [ -f "$RunID"_"$Sample_ID".bam ]; then
    # Then check that BAM is newer than the fastqs and scripts
    if [ "$RunID"_"$Sample_ID".bam -nt "$R1Filename" ] && \
       [ "$RunID"_"$Sample_ID".bam -nt "$R2Filename" ] && \
       [ "$RunID"_"$Sample_ID".bam -nt "$alignmentscript" ] && \
       [ "$RunID"_"$Sample_ID".bam -nt "$Sample_ID".variables ] && \
       [ "$RunID"_"$Sample_ID".bam -nt *.config ]
    then
        # If the BAM is newer there have been no changes and there is no
        # need to repeat the analysis, so we can exit here.
        logmessage "INFO: BAM file already exists and does not require repeating."
        exit 0
    fi
    # If they are newer, we need to repeat (e.g. script change)
    logmessage "WARNING: BAM file is older than prerequisite. Repeating analysis."
fi

# Fastq to BAM processing
# =======================


# Alignment
# ---------

# Split the cores between alignment and sorting
# 3:1 ratio appears to be optimal
sortntasks=$(( $alignmentntasks / 4 ))
bwantasks=$(( $alignmentntasks - $sortntasks ))

# DEV: Update these outputs to include datetime etc. for actual useful logging.
logmessage "INFO: Aligning "$Sample_ID"..."

# Align with BWA, while sorting and converting to BAM with samtools sort
bwa mem \
  -t "$bwantasks" \
  -R '@RG\tID:'"$RunID"'_'"$Sample_ID"'\tSM:'"$Sample_ID"'\tPL:'"$Platform"'\tLB:'"$ExperimentName" \
  "$bwarefgenome" \
  "$R1Filename" "$R2Filename" | \
  samtools sort \
    -@ "$sortntasks" \
    -O bam \
    -T "$javatmp" \
    -o "$ssdpath"/"$RunID"_"$Sample_ID"_aligned.bam

# Mark duplicates
# ---------------

# Mark duplicates (use Spark to leverage all available cores)
"$gatk" MarkDuplicatesSpark \
  --java-options "-Xmx12G -Djava.io.tmpdir="$javatmp"" \
  --tmp-dir "$javatmp" \
  --input "$ssdpath"/"$RunID"_"$Sample_ID"_aligned.bam \
  --create-output-bam-index true \
  --create-output-bam-splitting-index false \
  --spark-master local[*] \
  --output "$ssdpath"/"$RunID"_"$Sample_ID"_deduplicated.bam

# BQSR base-quality recalibration
# -------------------------------

# NOTE: There is NO difference between per-sample and per-run
#       level BQSR (at least in GATK4), so it may as well be run here
"$gatk" BaseRecalibratorSpark \
  --java-options "-Xmx8G -Djava.io.tmpdir="$javatmp"" \
  --tmp-dir "$javatmp" \
  --input "$ssdpath"/"$RunID"_"$Sample_ID"_deduplicated.bam \
  --reference "$refgenome" \
  --known-sites "$dbsnpfile" \
  --known-sites "$knownindels1" \
  --known-sites "$knownindels2" \
  --intervals "$BEDFilename" \
  --interval-padding 100 \
  --spark-master local[*] \
  --output "$ssdpath"/"$RunID"_"$Sample_ID"_recal_data.table

# Apply the recalibration
"$gatk" ApplyBQSRSpark \
  --java-options "-Xmx8G -Djava.io.tmpdir="$javatmp"" \
  --tmp-dir "$javatmp" \
  --input "$ssdpath"/"$RunID"_"$Sample_ID"_deduplicated.bam \
  --reference "$refgenome" \
  --create-output-bam-index true \
  --create-output-bam-splitting-index false \
  --bqsr-recal-file "$ssdpath"/"$RunID"_"$Sample_ID"_recal_data.table \
  --spark-master local[*] \
  --output "$RunID"_"$Sample_ID".bam

logmessage "SUCCESS: Alignment completed."
