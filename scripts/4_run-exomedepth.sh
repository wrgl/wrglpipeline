#!/bin/bash

#SBATCH --job-name=AnnotateVariants
#SBATCH --output=%x.%j.out
#SBATCH --error=%x.%j.err


# Script details
# ==============


# Description: WRGL Pipeline
# Stage: 3 - Merge and annotate variants
# Mode: BY_RUN


# Script config
# =============


# Load pipeline settings - load before variables and any duplicate valures
#                          will be replaced with the variables file ones.
. */WRGLPipeline.config

# Load sample variables
. */*.variables

cleanup(){
    # Run in event of error - delete temp files from /ssdfs
    # EXIT seems to be the most flexible option - it catches errors and ctrl+c
    # without issues, vs specifying EXIT and ERR, which can repeat itself.
    >&2 echo "DEV: Run cleanup here if needed..."
}
trap 'cleanup' EXIT

logmessage(){
    # Print a message to stderr, with date + time stamp
    message="$1"
    >&2 echo -e "$( date )\t$RunID\t$Sample_ID\t$message"
}

# Pre-processing checks
# =====================


# Check prior analyses have finished
# ----------------------------------

# We only want to run ExomeDepth if the final VCF is complete
# The complete file is created by the previous script, so it needs to be present
logmessage "INFO: Checking if VCF needs re-generating..."
if [ ! -f BAMsforDepthAnalysis.list ]; then
    logmessage "INFO: Variant processing step appears to be incomplete, skipping CNV calling."
    exit 0
fi


# Call CNVs with ExomeDepth
# =========================

# Run ExomeDepth
# --------------

# call CNVs using read depth
appexec "$rootdir"/software/exomedepth/exomedepth_1.1.16.sif \
Rscript "$installdir"/scripts/ExomeDepth.r \
-b BAMsforDepthAnalysis.list \
-f "$refgenome" \
-r "$installdir"/bed_files/WRGL-panel/data/WRGL5_hg19_v01_ExomeDepth.bed \
2>&1 | tee ExomeDepth.log

# move CNV VCFs to sample folder
for i in $(ls *_cnv.vcf); do
    "$gatk" UpdateVCFSequenceDictionary -V "$i" -O "$i".gz -R "$refgenome" --replace true
    add_tool "$i".gz ExomeDepth
    s=$(bcftools view -h "$i".gz | tail -n1 | awk '{print $10}')
    mv "$i".gz "$s"
    mv "$i".gz.tbi "$s"
    rm "$i"
done

# Merge CNV/SV/SNV/MEI
# =========================

for s in $(bcftools view -h "$RunID"_Filtered_Annotated.vcf | tail -n1 | cut -s -f10- | tr '\t' '\n'); do
    # subset SNVs by sample
    "$gatk" SelectVariants \
    -R "$refgenome" \
    -V "$RunID"_Filtered_Annotated.vcf \
    -O "$s"/"$RunID"_"$s"_Filtered_Annotated.vcf.gz \
    --sample-name "$s" \
    --exclude-non-variants

    # add TOOL
    add_tool "$s"/"$RunID"_"$s"_Filtered_Annotated.vcf.gz HaplotypeCaller

    # combine SNV/CNV/SV/MEI VCF files by sample
    "$gatk" MergeVcfs \
    -R "$refgenome" \
    $(ls "$s"/*.vcf.gz | grep -E "_ALU|_LINE1|_SVA|_cnv|_sv|_Filtered_Annotated" | sed 's/^/-I /' | tr '\n' ' ') \
    -O "$s"/"$RunID"_"$s"_Filtered_Annotated_All.vcf

    # validate final VCF
    "$gatk" ValidateVariants \
    -R "$refgenome" \
    -V "$s"/"$RunID"_"$s"_Filtered_Annotated_All.vcf

done

# package output files for download
zip -j "$RunID"_Filtered_Annotated_All.zip */*_Filtered_Annotated_All.vcf*

# create link for download
ln -s "$RunID"_Filtered_Annotated_All.zip "$RunID"_CNVs.report

>&2 echo CNV processing complete

# Complete the analysis
# =====================


# Write finish flag for download
echo > complete

# chmod everything again, so that all created files are acessible
# use || true to force error code 0 even if there's an error
chmod --silent -f -R 770 . || true
chgrp --silent -f -R wrgl . || true
