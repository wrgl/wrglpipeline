 WCEP and WRGL analysis scripts
================================

Scripts for the analyis of WCEP exome and WRGL panels samples on Iridis

## Usage

The data is automatically uploaded and the scripts run when the MiSeq run completes.
No manual intervention is required. This is handled by the C# MiSeq pipeline manager.

To manually trigger a run, first ensure that it is cleaned up and any changes to
run ID or folder name/location have been handled by running the `reset-run` script.
This is located in the scripts folder, and if the PATH subfolder has been added to
your system path (e.g. in .bashrc) it should be available on the command line.
Then, simply run the pipeline-runner.sh script in the WRGL-pipeline folder. This will
transfer all necessary files and submit the analysis jobs.

If you need to run a single sample, refer to the pipeline_runner.sh for an example
sbatch command for the WRGLPipeline.slurm script, or look up the job requirements
in WRGLPipeline.config. Default sbatch params will probably work just fine.

### nodes-to-exclude.txt

This file contains a list of known problem nodes that have caused the following problems:

 * Failure to write to /ssdfs
 * Extremely slow module loading (causing script timeouts)

These problems are likely due to the node confiuration, so may change. It is recommended
that these excluded nodes should be reviewed occasionally, and any new nodes causing problems
should be added.

The list is loaded by the pipeline_runner.sh script and used as an argument for the sbatch
job submission.

## Reference files

Note that the reference files and folders in this directory are links - this is to avoid putting
large amounts of data into GitLab. The scripts reference these folders, so if you update
the original files ensure that the links are changed to match.

The bed_files folder is an archive only - the BED file needed for a run is uploaded along
with the raw data from the live copy on the local Z: drive.

## Update history

### 3.2
Implemented CNV calling

### 3.1
Implemented SpliceAI

### 3.00
This pipeline uses the latest GATK4 release availble on Iridis 5 at the time of development.
All modules are configure in the WRGLPipeline.config file, and uses defined versions rather than the system default.

There are 4 main stages:

* Alignment
* Variant calling
* Variant filtering and annotation
* CNV calling with ExomeDepth

ExomeDepth is a development only step at the moment.

The pipeline is initiated by pipeline_runner.sh, with WRGLPipeline.slurm being the submitted job.
For Iridis 5 we now use sbatch for job submission.

### 2.21

 * Adds pipeline_runner.sh to run QC scripts on login node
 * Adds call to backup run ready for Amazon Glacier upload (not yet implemented)

### 2.2

 * Combined scripts with the WRGL panels
 * Introduced dynamic PBS resource requests

### 2.1

 * Introduced config file to define pipeline parameters (software paths etc.)
